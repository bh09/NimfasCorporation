<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/asset/include/headBase.jsp" %>
        <title>Nimfas Co. Casino</title>
        <style><%@include file="/WEB-INF/asset/include/css/login.css"%></style>
    </head>
    <body class="bg-dark">
        <%@include file="/WEB-INF/asset/include/navbarMainPage.jsp" %>
        <div id="indexMainDiv">
            <div class="container">
                <form action="j_security_check" class="form-horizontal text-light" method="POST">
                    <div class="form-group row">
                        <label for="exampleInputEmail1">Email-cím</label>
                        <input name="j_username" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="adja meg email-címét">
                    </div>
                    <div class="form-group row">
                        <label for="exampleInputPassword1">Jelszó</label>
                        <input name="j_password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Jelszó">
                    </div>
                    <button type="submit" class="btn btn-primary">Belépés</button>
                </form>
            </div>
        </div>
    </body>
</html>
