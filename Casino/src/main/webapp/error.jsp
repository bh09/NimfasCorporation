<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="WEB-INF/asset/include/headBase.jsp" %>
        <title>Nimfas Co. Casino</title>
        <style><%@include file="/WEB-INF/asset/include/css/blackjack.css"%></style>
    </head>
    <body>
        <%@include file="/WEB-INF/asset/include/navbarMainPage.jsp" %>
        <div class="container">
            <div class="row">
                <h1>Hibás felhasználónév vagy jelszó!</h1>
                </br>
                <a class="btn btn-danger" href="index.jsp" role="button">Vissza</a>
            </div>
        </div>
    </body>
</html>
