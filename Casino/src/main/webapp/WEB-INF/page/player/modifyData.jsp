<%-- 
    Document   : registration
    Created on : 2019.06.18., 17:57:37
    Author     : Zsófi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsPlayer"%>
<!DOCTYPE html>
<html>
    <head>

        <%@include file="../../asset/include/headBase.jsp" %>
        
    </head>
     <body class = "bg-dark">
        <%@include file="../../asset/include/navbarPlayer.jsp" %>
        
        <div class="container">
            <form class="form-horizontal text-light" action='' method="POST">
                <fieldset>
                    <div id="legend">
                        <legend class="text-light">Adatok módosítása</legend>
                        
                    </div>
                    
                     <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Nick név: </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputUsername" >
                        </div>
                    </div>

                  <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Régi jelszó:* </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword" placeholder="" required="required">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Új jelszó:* </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword" placeholder="" required="required">
                        </div>
                    </div>
                    
                     <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Új jelszó mégegyszer:* </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword" placeholder="" required="required">
                        </div>
                    </div>
                    
                    


                    <div class="form-group row"><label class="col-sm-2 col-form-label" >Profilkép feltöltése</label>
                        <form method = "post" action = ${pageContext.request.contextPath}  enctype="multipart/form-data">
                            <div class="col-sm-10">
                                <input type="file" name ="file" value ="select images..."/>
                                <input type="submit" value="Feltölt"/>
                        </form>
                    </div> 
                    </div>
                    
                    <div class="control-group">
                        <!-- Button -->
                        <div class="controls">
                            <button class="btn btn-danger">Rögzít</button>
                        </div>
                    </div>
                           
                </fieldset>
            </form>
        </div>
    </body>
</html>
