<%-- 
    Document   : admin
    Created on : 2019.06.19., 22:12:47
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@page import="hu.nimfasco.casino.constant.HtmlNameConstantsAdmin"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../../asset/include/headBase.jsp" %>
    </head>
    <body class="bg-dark">
        <%@include file="../../asset/include/navbarAdmin.jsp" %>

        <div class="container">
            <form>
                <div class="form-group">
                    <label class="text-light">Feladó</label>
                    <input type="email" class="form-control" name="<%= HtmlNameConstantsAdmin.FROM_EMAIL_ADDRESS%>" placeholder="Feladó email címe">
                </div>
                <div class="form-group">
                    <label class="text-light">Címzett</label>
                    <input type="email" class="form-control" name="<%= HtmlNameConstantsAdmin.TO_EMAIL_ADDRESS%>" placeholder="Címzett email címe">
                </div>
                <div class="form-group">
                    <label class="text-light">Üzenet szövege</label>
                    <input type="text" class="form-control" name="<%= HtmlNameConstantsAdmin.EMAIL_MESSAGE%>" placeholder="Ide írhatja a levelet">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Meghívás</button>
                </div>

            </form>
        </div> 
    </body>
</html>
