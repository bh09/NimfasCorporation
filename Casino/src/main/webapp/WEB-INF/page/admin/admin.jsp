<%-- 
    Document   : admin
    Created on : 2019.06.19., 22:12:47
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../../asset/include/headBase.jsp" %>
    </head>
    <body class="bg-dark">
        <%@include file="../../asset/include/navbarAdmin.jsp" %>

        <div class="container">
            <h1 class="text-light">Online Casino User lista</h1>
            <table class="table table-hover table-dark">
                <thead>
                    <tr>
                        <th scope="col">User név</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Regisztráció ideje</th>
                        <th scope="col">Utolsó bejelentkezés</th>
                        <th scope="col">Egyenleg</th>
                        <th scope="col">Profilkép</th>
                        <th scope="col">Ban</th>
                    </tr>
                </thead>
                <tbody>

                    <c:forEach items="${userDetail}" var="ud">
                        <tr>
                            <td>${ud.playerName}</td>
                            <td>${ud.getUserDto().getEmail()}</td>
                            <td>${ud.getRegistrationTime()}</td>
                            <td>${ud.getLastLogin()}</td>
                            <td>${ud.getBalance()}</td>
                            <td>${ud.getPhotopath()}</td>

                            <td> <style>
                                    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
                                    .toggle.ios .toggle-handle { border-radius: 20rem; }
                                </style>
                                <input  type="checkbox" checked data-toggle="toggle" data-style="ios"
                                        data-size="normal" 
                                        data-onstyle="danger" data-offstyle="success"
                                        data-on="Ban" data-off="Unban">
                            </td>


                        </tr>  
                    </c:forEach>




                </tbody>
            </table>
        </div>
    </body>
</html>
