<%-- 
    Document   : blackjack
    Created on : 2019.06.28., 16:13:34
    Author     : vbali
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../../asset/include/headBase.jsp" %>
        <title>Nimfas-BlackJack</title>
        <style><%@include file="/WEB-INF/asset/include/css/blackjack.css"%></style>
    </head>
    <body class="bg-dark" background="${pageContext.request.contextPath}/images/table-background.jpg">
        <div class="container" style="height: 200px;">
            <div class="h-50 row">
                <div class="col">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>Játékos neve</th>
                                <th>Aktuális egyenleg</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Misi</td>
                                <td>${playerCash}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col"></div>
                <div class="col">
                    <h5>Ha kicsi a tét, a kedvem sötét!</h5>
                </div>
            </div>
            <div class="h-100 row">
                <div class="col"></div>
                <div class="col">
                    <h5>BANK</h5>
                </div>
                <div class="col-7">
                    <c:forEach items="${dealerCards}" var="card">
                        <img src="${pageContext.request.contextPath}/images/karten/<c:out value="${card.value}${card.color}"/>.png">
                    </c:forEach>
                    <c:if test="${dealerCards.size() >= 1}">
                        <c:if test="${isGameInProgress}">
                            <img src="${pageContext.request.contextPath}/images/karten/red_back.png">
                        </c:if>
                   </c:if>
                </div>
                <div class="col"></div>
            </div>
            <div class="h-100 row">
                <div class="col"></div>
                <div class="col">
                    <h5>PLAYER</h5>
                </div>
                <div class="col-7">
                    <c:forEach items="${playerCards}" var="card">
                        <img src="${pageContext.request.contextPath}/images/karten/<c:out value="${card.value}${card.color}"/>.png">
                    </c:forEach>
                </div>
                <div class="col"><h1>${state}</h1></div>
            </div>

            <div class="row">
                <div class="col">
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <th>Aktuális tét:</th>
                                <td>${bet}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-7">
                    <form class="form-horizontal text-light" action='' method="POST">
                        <input type="submit" name="increaseBet" value="NAGYOBB TÉT" class="btn btn-success" />
                        <input type="submit" name="decreaseBet" value="KISEBB TÉT" class="btn btn-danger" />
                        <input type="submit" name="deal" value="OSZTÁS" class="btn btn-warning" />
                        <input type="submit" name="hit" value="HÚZÁS" class="btn btn-primary" />
                        <input type="submit" name="stand" value="ÁLLJ" class="btn btn-primary" />
                        <input type="submit" name="exit" value="KILÉPÉS" class="btn btn-secondary" />
                    </form>
                </div>
                <div class="col"></div>
            </div>

        </div>
    </body>
</html>
