<%-- 
    Document   : newGame
    Created on : 2019.06.22., 1:10:08
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <%@page import="hu.nimfasco.casino.constant.WebPagesConstantsGame"%>
    <head>
        <%@include file="../../asset/include/headBase.jsp" %>
        <title>Játék kiválasztása</title>
    </head>
    <body class="bg-dark text-light">
        <%@include file="../../asset/include/navbarPlayer.jsp" %>
        <div class="container">
            <h1>Játék kiválasztása</h1>
            <div class="row">
                <div id ="blackjack-to-bg" class="col-md-6 border border-secondary"> 
                    
                    <div class="card-body">
                        <p class="card-text">Blackjack</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href=".<%= WebPagesConstantsGame.URL_PATTERN_BLACKJACK%>"><button class="btn btn-danger" type="submit">Kiválasztás</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 border border-secondary">               
                    <div class="card-body">
                        <p class="card-text">Kockapóker (fejlesztés alatt)</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#"><button class="btn btn-danger" type="submit" disabled>Kiválasztás</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
