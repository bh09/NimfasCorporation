<%-- 
    Document   : registration
    Created on : 2019.06.18., 17:57:37
    Author     : Zsófi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <%@include file="../../asset/include/headBase.jsp" %>
        <script src="https://www.google.com/recaptcha/api.js"></script>
        
    </head>
    <body class = "bg-dark">
        <%@include file="../../asset/include/navbarMainPage.jsp" %>

        <div class="container">
            <form class="form-horizontal text-light" method="POST" action = "#" enctype="multipart/form-data">
                <fieldset>
                    <div id="legend">
                        
                        <legend class="text-light">Regisztráció</legend>
                    </div>

                    <div class="form-group row">
                        <label for="inputUsername" class="col-sm-2 col-form-label">Nick név:* </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name = "username" id="username" required="required">
                        </div>
                    </div>
                    
                     <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">E-mail cím:* </label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name ="email" placeholder="address@email.com" required="required">
                        </div>
                    </div>
                     
                    <div class="form-group row">
                        <label for="inputDate" class="col-sm-2 col-form-label">Születési dátum:* </label>
                        <div class="col-sm-10" id="datumdiv">
                          
                        </div>
                    </div>
                       

                  <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Jelszó:* </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name ="password" id="password" placeholder="" required="required">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Jelszó mégegyszer:* </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password2" name ="password2" placeholder="" required="required">
                           <div><c:out   value= "${ passwordcompareerror }" /></div>
                            
                           
                        
                        </div>
                    </div>
                    
                    
                    <div class="form-group row"><label class="col-sm-2 col-form-label" >Profilkép feltöltése</label>
                       
                            <div class="col-sm-10">
                                <input type="file" name ="file" id="file" value ="select images..."/>
                        
                    </div> 
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="checkbox"  required="required"<label > Elfogadom az <a href="../../../Casino/info/adatvedelmiszabalyzat.pdf" target="_blank">adatvédelmi nyilatkozatot</a><br/><br/></label>

                        </div>
                    </div>
                            
                    <div class="g-recaptcha"
                         data-sitekey="6LfOJKoUAAAAAGT0hjIVIsOKFsyFp7Wt1mXSgR51">
                          
                          
                    </div>
                    <div><c:out   value= "${ captchaerror }" /></div>
                    <div class="control-group">

                        <div class="controls">
                            <button class="btn btn-danger">Regisztráció</button>
                        </div>
                    </div>
                            
                   
                </fieldset>
            </form>
        </div> 
                    <script type="text/javascript">
                        var str = (new Date().getFullYear () - 18) + "-" + (new Date().getMonth ()<10 ? "0" : "") + (new Date().getMonth () + 1) + "-" + (new Date().getDay() + 16 < 10 ? "0" : "") + (new Date().getDay () + 16);

                        document.getElementById("datumdiv").innerHTML = '<input type="date" id="datum" min = "1901-01-01" max = "' + str +'" class="form-control" name ="datum" required="required">';
                       
        </script>
    </body>
</html>

