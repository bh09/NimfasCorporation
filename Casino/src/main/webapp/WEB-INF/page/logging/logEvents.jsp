<%-- 
    Document   : logEvents
    Created on : 2019.07.09., 23:49:54
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="../../asset/include/headBase.jsp" %>
        <title>Log események</title>
    </head>
    <body class="bg-dark">
        <%@include file="../../asset/include/navbarAdmin.jsp" %>

        <div class="container">
            <h1 class="text-light">Log események</h1>


            <form method="POST">
                <select name="select_level">
                    <option name="option_level" value="INFO">INFO</option>
                    <option name="option_level" value="DEBUG">DEBUG</option>
                </select>
                <input type="submit" name="filter" value="Szűrés" class="btn btn-success" />
            </form>

            <table class="table table-hover table-dark">
                <thead>
                    <tr>
                        <td scope="col">Időpont</td>
                        <td scope="col">Log level</td>
                        <td scope="col">Java osztály</td>
                        <td scope="col">Log üzenet</td>
                    </tr>
                </thead>
                <c:forEach items="${logEventList}" var="i">
                    <tr>
                        <td>${i.logDateTime.toString()}</td>
                        <td>${i.logLevel}</td>
                        <td>${i.loggedClass}</td>
                        <td>${i.logMessage}</td>
                    </tr>    
                </c:forEach>
            </table>
        </div>
    </body>
</html>
