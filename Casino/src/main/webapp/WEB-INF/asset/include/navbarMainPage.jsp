<%-- 
    Document   : navbarMainPage
    Created on : 2019.06.19., 22:25:12
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsIndex"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsGame"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsRegistration"%>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark"> 
        <div class="col-6"> 
            <a class="navbar-brand" href="<%= WebPagesConstantsIndex.URL_PATTERN_INDEX%>">Nimfas Co. Casino</a> 
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="col-6">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href=".<%= WebPagesConstantsRegistration.URL_PATTERN_REGISTRATION%>">Regisztráció</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=".<%= WebPagesConstantsGame.URL_PATTERN_NEW_GAME%>">Bejelentkezés</a>
                    </li>
                </ul>
            </div>
    </nav>
</div>