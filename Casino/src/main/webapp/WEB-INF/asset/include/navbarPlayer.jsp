<%-- 
    Document   : navbarPlayer
    Created on : 2019.06.22., 22:54:28
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsLogin"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsIndex"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsGame"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsPlayer" %>
<%@page import="hu.nimfasco.casino.constant.HtmlNameConstantsGame" %>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<%= WebPagesConstantsIndex.URL_PATTERN_INDEX%>">Nimfas Co. Casino</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls=" navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsPlayer.URL_PATTERN_MODIFYDATA%>">${pageContext.request.remoteUser}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsPlayer.URL_PATTERN_MODIFYDATA%>">Adatok módosítása</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsLogin.URL_PATTERN_LOGOUT%>">Kilépés</a>
                </li>
            </ul>
    </nav>
</div>
