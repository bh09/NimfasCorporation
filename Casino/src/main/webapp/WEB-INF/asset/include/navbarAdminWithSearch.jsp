<%-- 
    Document   : navbarAdmin
    Created on : 2019.06.19., 22:27:53
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsIndex"%>
<%@page import="hu.nimfasco.casino.constant.WebPagesConstantsAdmin"%>
<%@page import="hu.nimfasco.casino.constant.HtmlNameConstantsAdmin"%>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Nimfas Co. Casino</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls=" navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsAdmin.URL_PATTERN_ADMIN%>">admin_user_name </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsAdmin.URL_PATTERN_ADMIN_SEND_EMAIL%>">Email küldés</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=".<%= WebPagesConstantsAdmin.URL_PATTERN_ADMIN_SEND_INVITATION%>">Meghívó küldés</a>
                </li>
                <%@include file="../../asset/include/navbarItemSearch.jsp" %>
                <li class="nav-item">
                    <form name="logoutAdmin" class="form-inline my-2 my-lg-0">
                        <a class="nav-link" href="<%= WebPagesConstantsIndex.URL_PATTERN_INDEX%>">Kilépés</a>
                    </form>
                </li>
            </ul>
    </nav>
</div>
