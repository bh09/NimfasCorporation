<%-- 
    Document   : navbarItemSearch
    Created on : 2019.06.20., 1:14:31
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<li class="nav-item">
    <form class="form-inline my-2 my-lg-0">
        <input name="searchUser" class="form-control mr-sm-2" type="search" placeholder="User keresése"
               aria-label="userSearch">
        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Keresés</button>
    </form>
</li>
