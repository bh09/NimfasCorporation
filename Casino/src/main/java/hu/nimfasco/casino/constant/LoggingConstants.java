package hu.nimfasco.casino.constant;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author horvathbzs
 */
public class LoggingConstants {
    
    public static final String PATH_LOGFILE = "C:\\Casino\\log\\log.txt";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
}
