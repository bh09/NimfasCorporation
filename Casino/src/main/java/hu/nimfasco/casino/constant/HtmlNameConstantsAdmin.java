package hu.nimfasco.casino.constant;

/**
 *
 * @author horvathbzs
 */
public class HtmlNameConstantsAdmin {

    public static final String FROM_EMAIL_ADDRESS = "fromEmailAddress";
    public static final String TO_EMAIL_ADDRESS = "toEmailAddress";
    public static final String CC_EMAIL_ADDRESS = "ccEmailAddress";
    public static final String BCC_EMAIL_ADDRESS = "bccEmailAddress";
    public static final String EMAIL_SUBJECT = "emailSubject";
    public static final String EMAIL_MESSAGE = "emailMessage";
    
    public static final String LOGOUT_ADMIN = "logoutAdmin";
}
