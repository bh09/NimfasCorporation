package hu.nimfasco.casino.constant;

/**
 *
 * @author horvathbzs
 */
public class WebPagesConstantsLogging {
    
    public static final String PATH_LOG_EVENT = "./WEB-INF/page/logging/logEvents.jsp";
    public static final String SERVLET_LOG_EVENT = "LogEventServlet";
    public static final String URL_PATTERN_LOG_EVENT = "/logevents";
}
