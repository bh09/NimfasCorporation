/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.constant;

/**
 *
 * @author Zsófi
 */
public class WebPagesConstantsPlayer {

    public static final String PATH_MODIFYDATA = "./WEB-INF/page/player/modifyData.jsp";
    public static final String SERVLET_MODIFYDATA = "ModifyDataServlet";
    public static final String URL_PATTERN_MODIFYDATA = "/modifydata";
}
