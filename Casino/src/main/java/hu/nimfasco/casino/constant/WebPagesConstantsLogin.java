/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.constant;

/**
 *
 * @author vbali
 */
public class WebPagesConstantsLogin {
    
    public static final String SERVLET_GATEWAY = "LoginGateway";
    public static final String URL_PATTERN_GATEWAY = "/logingateway";
    
    public static final String SERVLET_LOGOUT = "LogoutServlet";
    public static final String URL_PATTERN_LOGOUT = "/logingout";

}
