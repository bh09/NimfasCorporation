package hu.nimfasco.casino.constant;

/**
 *
 * @author horvathbzs
 */
public class WebPagesConstantsAdmin {

    public static final String PATH_ADMIN = "./WEB-INF/page/admin/admin.jsp";
    public static final String SERVLET_ADMIN = "AdminServlet";
    public static final String URL_PATTERN_ADMIN = "/admin";

    public static final String PATH_ADMIN_SEND_EMAIL = "./WEB-INF/page/admin/adminSendEmail.jsp";
    public static final String SERVLET_ADMIN_SEND_EMAIL = "AdminSendEmailServlet";
    public static final String URL_PATTERN_ADMIN_SEND_EMAIL = "/adminsendemail";

    public static final String PATH_ADMIN_SEND_INVITATION = "./WEB-INF/page/admin/adminSendInvitation.jsp";
    public static final String SERVLET_ADMIN_SEND_INVITATION = "AdminSendInvitationServlet";
    public static final String URL_PATTERN_ADMIN_SEND_INVITATION = "/adminsendinvitation";
}
