/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.constant;

/**
 *
 * @author Zsófi
 */
public class WebPagesConstantsRegistration {

    public static final String PATH_REGISTRATION = "./WEB-INF/page/registration/registration.jsp";
    public static final String SERVLET_REGISTRATION = "RegistrationServlet";
    public static final String URL_PATTERN_REGISTRATION = "/registration";
    
    public static final String PATH_DATA = "/info/data.html";
}
