package hu.nimfasco.casino.constant;

/**
 *
 * @author horvathbzs
 */
public class WebPagesConstantsGame {

    public static final String PATH_NEW_GAME = "./WEB-INF/page/game/newGame.jsp";
    public static final String SERVLET_NEW_GAME = "NewGameServlet";
    public static final String URL_PATTERN_NEW_GAME = "/newgame";
    
    public static final String PATH_BLACKJACK = "./WEB-INF/page/game/blackjack.jsp";
    public static final String SERVLET_BLACKJACK = "BlackJackServlet";
    public static final String URL_PATTERN_BLACKJACK = "/blackjack";
}
