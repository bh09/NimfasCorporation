package hu.nimfasco.casino.util;

/**
 *
 * @author horvathbzs
 */
public class RandomGenerator {
    public static int randomInt(int from, int to) {    
        return (int) (Math.random() * to + from);
    }
    
    public static int randomInt(int to) {    
        return (int) (Math.random() * to + 1);
    }
}
