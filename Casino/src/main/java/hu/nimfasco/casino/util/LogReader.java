package hu.nimfasco.casino.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author horvathbzs
 */
public class LogReader {
      
    public static List<String> readLogFile(String filePath) {
        
        File logFile = new File(filePath);
        
        List<String> linesFromLogFile = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(logFile))) { 
            String lineInFile;
            while ((lineInFile = br.readLine()) != null) {
                linesFromLogFile.add(lineInFile);
            }     
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return linesFromLogFile;
    }
    
    
}
