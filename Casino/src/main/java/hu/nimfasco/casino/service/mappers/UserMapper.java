/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.service.mappers;

import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.entity.Role;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author vbali
 */
public class UserMapper {
    
    public static User mapUserFromUserDto(UserDto userDto) {
        
        User user = new User(userDto.getEmail());
        Role role = new Role(user, "player");
        user.setPassword(userDto.getPassword());
        user.setRole(role);
        UserDetail userDetail = mapUserDetailFromUserDetailDto(userDto.getDetails());
        userDetail.setUser(user);
        user.setDetails(userDetail);
        
        return user;
        
    }
    
    public static UserDetail mapUserDetailFromUserDetailDto(UserDetailDto udeDto) {
        
        UserDetail userDetail = new UserDetail();
        userDetail.setBalance(udeDto.getBalance());
        userDetail.setBanned(udeDto.isBanned());
        userDetail.setLastLogin(udeDto.getLastLogin());
        userDetail.setRegistrationTime(udeDto.getRegistrationTime());
        userDetail.setPhotopath(udeDto.getPhotopath());
        userDetail.setPlayerName(udeDto.getPlayerName());
        return userDetail;
    }
    
}
