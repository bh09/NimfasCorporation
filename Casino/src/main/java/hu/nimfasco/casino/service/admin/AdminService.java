/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.service.admin;

import hu.nimfasco.casino.dao.impl.RoleDao;
import hu.nimfasco.casino.dao.impl.UserDao;
import hu.nimfasco.casino.dao.impl.UserDetailDao;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.entity.Role;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import hu.nimfasco.casino.service.mappers.AdminMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Misi
 */
@Singleton
public class AdminService {
    
    

    @Inject
    private UserDao userDao;
    
    @Inject
    private UserDetailDao userDetailDao;

    public void savePlayer(UserDetailDto userDetailDto) {
        userDetailDao.save(AdminMapper.mapUserDetailDtoToUserDetail(userDetailDto));
    }

    public void banPlayer(UserDetailDto userDetailDto) {

        if (userDetailDto.isBanned() == true) {
            userDetailDto.setBanned(false);
        } else {
            userDetailDto.setBanned(true);
        }
        savePlayer(userDetailDto);
    }
    
    public List<UserDetailDto> getPlayers(){
        List <UserDetailDto> playerDtos = new ArrayList<>();
        List <UserDetail> userDetails = new ArrayList<>();
        userDetails.addAll(userDetailDao.getPlayers());
        for (int i = 0; i < userDetails.size(); i++) {
            playerDtos.add(AdminMapper.mapUserDetailToUserDetailDto(userDetails.get(i)));   
            System.out.println(playerDtos.get(i));
        }
        return playerDtos;
        
    }
    
    

}
