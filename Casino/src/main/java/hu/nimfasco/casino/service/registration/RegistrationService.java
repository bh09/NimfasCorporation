package hu.nimfasco.casino.service.registration;

import hu.nimfasco.casino.dao.impl.UserDao;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.service.mappers.UserMapper;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import javax.ejb.Singleton;
import javax.inject.Inject;


/**
 *
 * @author horvathbzs
 */
@Singleton
public class RegistrationService {
    
    public static final int INITIAL_BALANCE = 2000;
    
    @Inject
    private UserDao userDao;

    public void savePlayer(UserDto userDto) throws IllegalAccessException, InvocationTargetException {
        userDao.save(UserMapper.mapUserFromUserDto(userDto));
    }
    
    public UserDto setUserDto(String email, String password, UserDetailDto udeDto) {
        
        UserDto userDto = new UserDto();
        
        userDto.setEmail(email);
        userDto.setPassword(password);
        udeDto.setUserDto(userDto);
        userDto.setDetails(udeDto);

        return userDto;
    }
    
    public UserDetailDto setUserDetailDto(String playerName, String photopath) {
        
        UserDetailDto udeDto = new UserDetailDto();
        
        udeDto.setPlayerName(playerName);
        udeDto.setPhotopath(photopath);
        udeDto.setBalance(INITIAL_BALANCE);
        udeDto.setBanned(false);
        udeDto.setLastLogin(LocalDate.now());
        udeDto.setRegistrationTime(LocalDate.now());

        return udeDto;
    }

}
