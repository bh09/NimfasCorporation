package hu.nimfasco.casino.service.logging;

import hu.nimfasco.casino.constant.LoggingConstants;
import hu.nimfasco.casino.dto.LogEventDto;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;

/**
 *
 * @author horvathbzs
 */
@Singleton
public class LogEventService {

    private static final String LOG_EVENT_SEPARATOR = "--";
    
    public List<LogEventDto> readLogFile(String filePath) {

        List<LogEventDto> logEvents = new ArrayList<>();

        File logFile = new File(filePath);

        try (BufferedReader br = new BufferedReader(new FileReader(logFile))) {

            String lineInFile;

            while ((lineInFile = br.readLine()) != null) {

                String[] splitLine = lineInFile.split(LOG_EVENT_SEPARATOR);
                LocalDateTime localDateTime
                        = LocalDateTime.parse(splitLine[0].trim(), LoggingConstants.DATE_TIME_FORMATTER);

                LogEventDto logEventDto = new LogEventDto(
                        localDateTime,
                        splitLine[1].trim(),
                        splitLine[2].trim(),
                        splitLine[3].trim());
                
                logEvents.add(logEventDto);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return logEvents;
    }

    public List<LogEventDto> getLogEventsByLevel(List<LogEventDto> logEvents, String level) {
        return logEvents.stream()
                .filter(logEvent -> level.equals(logEvent.getLogLevel()))
                .collect(Collectors.toList());
    }
}
