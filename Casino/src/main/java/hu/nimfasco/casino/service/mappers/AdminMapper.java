/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.service.mappers;

import hu.nimfasco.casino.dao.impl.UserDetailDao;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.entity.Role;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import static hu.nimfasco.casino.service.mappers.UserMapper.mapUserDetailFromUserDetailDto;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Misi
 */
public class AdminMapper {
    
    public static UserDetail mapUserDetailDtoToUserDetail(UserDetailDto userDetailDto) {
        
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
       //? role.setRoleId((long) -1);
        role.setRoleName("player");
        roleSet.add(role);
                
        UserDetail userDetail = new UserDetail();
        userDetail.setUser(new User());
        userDetail.getUser().setEmail(userDetailDto.getUserDto().getEmail());
        userDetail.setBalance(userDetailDto.getBalance());
        userDetail.setBanned(userDetailDto.isBanned());
        userDetail.setLastLogin(userDetailDto.getLastLogin());
        userDetail.setRegistrationTime(userDetailDto.getRegistrationTime());
        userDetail.setPhotopath(userDetailDto.getPhotopath());
        userDetail.setPlayerName(userDetailDto.getPlayerName());
        
        return userDetail;
    }
       
    public static UserDetailDto mapUserDetailToUserDetailDto(UserDetail userDetail){
    
        UserDetailDto userDetailDto = new UserDetailDto();
        userDetailDto.setUserDto(new UserDto());
        userDetailDto.getUserDto().setEmail(userDetail.getUser().getEmail());
        userDetailDto.setBalance(userDetail.getBalance());
        userDetailDto.setLastLogin(userDetail.getLastLogin());
        userDetailDto.setPhotopath(userDetail.getPhotopath());
        userDetailDto.setPlayerName(userDetail.getPlayerName());
        userDetailDto.setRegistrationTime(userDetail.getRegistrationTime());
        userDetailDto.setBanned(userDetail.isBanned());
        
        return userDetailDto;
    }
    
//    public static List<UserDetailDto> mapUserDetailListToUserDetailDtoList (List<UserDetail> userDetails, UserDetailDao userDetailDao, UserDetail userDetail){
//        List<UserDetailDto> userDetailDtos = new ArrayList<>();
//        
//        userDetails.addAll(userDetailDao.getPlayers());
//        
//        for (int i = 0; i < userDetails.size(); i++) {
//            
//            userDetailDtos.add(mapUserDetailToUserDetailDto(userDetail));
//            
//        }
//        
//        return userDetailDtos;
//    }
    
    public static UserDetail mapUserDetailDtoToUserDetailToregisterAdmin(UserDetailDto userDetailDto) {
        
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
     //?   role.setRoleId((long) -2);
        role.setRoleName("admin");
        roleSet.add(role);
                
        UserDetail userDetail = new UserDetail();
        userDetail.setUser(new User());
        userDetail.getUser().setEmail(userDetailDto.getUserDto().getEmail());
        userDetail.setBalance(userDetailDto.getBalance());
        userDetail.setBanned(userDetailDto.isBanned());
        userDetail.setLastLogin(userDetailDto.getLastLogin());
        userDetail.setRegistrationTime(userDetailDto.getRegistrationTime());
        userDetail.setPhotopath(userDetailDto.getPhotopath());
        userDetail.setPlayerName(userDetailDto.getPlayerName());
        
        return userDetail;
    }
    
    
}
