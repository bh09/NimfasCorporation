/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.entity;

import hu.nimfasco.casino.dto.UserDetailDto;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vbali
 */
@Entity
@Table(name="user_detail")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "detail_id")
    private Long detailId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    
    @Column(name = "player_name")
    private String playerName;
    
    @Column(name = "registration_time")
    private LocalDate registrationTime;
    
    @Column(name = "last_login")
    private LocalDate lastLogin;
    
    @Column(name = "balance")
    private double balance;
   
    @Column(name = "photopath")
    private String photopath;
    
    @Column(name = "banned")
    private boolean isBanned;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userDetail", fetch = FetchType.EAGER)
    List<Session> session;

    public UserDetail(UserDetailDto userDetailDto) {
       balance = userDetailDto.getBalance();
       // mindent át kell másolni, de mivel a userDetailDTO-ban UserDTO van 
       //ezért majd kell egy ilyen konstruktor a usernek is ami userDto-ból szed ki adatokat
        //mivel itt user van, nem pedig userDto
    }
    
}
