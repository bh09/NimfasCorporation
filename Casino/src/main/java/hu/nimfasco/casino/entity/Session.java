
package hu.nimfasco.casino.entity;

import hu.nimfasco.casino.gameEnum.GameEnum;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="sessions")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "session_id")
    private Long sessionId;

   
    @ManyToOne
    @JoinColumn(name = "userDetail_id")
    private UserDetail userDetail;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "game")
    private GameEnum game;
    
    @Column(name = "session_start")
    private LocalDate sessionStart;
    
    @Column(name = "session_end")
    private LocalDate sessionEnd;
    
    @Column(name = "session_achivement")
    private double sessionAchivement;

    public void setGameId(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
