package hu.nimfasco.casino.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogEventDto {
    
    private LocalDateTime logDateTime;
    private String logLevel;
    private String loggedClass;
    private String logMessage;
}
