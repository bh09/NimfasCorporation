/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vbali
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    
    private UserDto user;
    private String roleName;
}
