/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dto;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vbali
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailDto {
    
    private UserDto userDto;
    private String playerName;
    private LocalDate registrationTime;
    private LocalDate lastLogin;
    private double balance;
    private String photopath;
    private boolean isBanned;
    List<SessionDto> session;
}
