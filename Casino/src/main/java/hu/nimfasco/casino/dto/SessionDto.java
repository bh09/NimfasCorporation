/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dto;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vbali
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionDto {
    
    private UserDetailDto user;
    private int gameId;
    private LocalDate sessionStart;
    private LocalDate sessionEnd;
    private double sessionAchivement;
    
    //deprecated
    public SessionDto(int gameId, LocalDate sessionStart, double sessionAchivement, UserDetailDto user) {
        this.gameId = gameId;
        this.sessionStart = sessionStart;
        this.sessionAchivement = sessionAchivement;
        this.user = user;
    }
    
    
}
