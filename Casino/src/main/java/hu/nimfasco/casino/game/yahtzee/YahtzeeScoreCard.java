package hu.nimfasco.casino.game.yahtzee;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class YahtzeeScoreCard {
    
    private List<YahtzeeRound> yathzeeRounds = new ArrayList<>();
    
    public void add(YahtzeeRound yahtzeeRound) {
        yathzeeRounds.add(yahtzeeRound);
    }
}
