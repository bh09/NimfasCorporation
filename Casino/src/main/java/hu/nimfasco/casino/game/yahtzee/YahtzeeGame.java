package hu.nimfasco.casino.game.yahtzee;

import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.game.yahtzee.type.YahtzeeBoxValue;
import hu.nimfasco.casino.util.RandomGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author horvathbzs
 */
public class YahtzeeGame {
    
    private final Integer DICE_SIDE_NUM = 6;
    private final Integer DICE_IN_HAND = 5;
    private final Integer MAX_ROLLS = 3;
    
    List<Integer> diceToChangeIndices = new ArrayList<>();
    Map<Integer, Integer> distinctDiceCount = new HashMap<>();
    
    
    public void yahtzeeGame() {
    }
    
    public void init() {
        DiceHand diceHand = createHand(DICE_IN_HAND);
        addDiceToChangeIndex(0);
        addDiceToChangeIndex(3);
        
        
        
        
    }
    
    public Dice rollOneDice() {
        Dice dice = new Dice();
        dice.setNumOfSides(DICE_SIDE_NUM);
        dice.setValue(dice.roll());
        return dice;
    }
    
    public DiceHand createHand(Integer diceInHand) {
        DiceHand diceHand = new DiceHand();
        for (int i = 0; i < DICE_IN_HAND; i++) {
            diceHand.add(rollOneDice());
        }
        return diceHand;
    }
    
    public void modifyHand(DiceHand diceHand, List diceToChangeIndices) {
        for (int i = 0; i < diceToChangeIndices.size(); i++) {
            diceHand.changeOneDiceInHand((Integer)diceToChangeIndices.get(i));
        }
    }
    
    public void addDiceToChangeIndex(Integer index) {
        diceToChangeIndices.add(index);
    }
}
