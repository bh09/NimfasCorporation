/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;
 
/**
 *
 * @author Zsófi
 */
public class Card {
    CardEnum value;
    
    ColorEnum color;

    @Override
    public String toString() {
        return "Card{" + "value=" + value + ", color=" + color + '}';
    }

    public Card(CardEnum value, ColorEnum color) {
        this.value = value;
        this.color = color;
    }

    
    public CardEnum getValue() {
        return value;
    }

    public void setValue(CardEnum value) {
        this.value = value;
    }

    public ColorEnum getColor() {
        return color;
    }

    public void setColor(ColorEnum color) {
        this.color = color;
    }
    
}
