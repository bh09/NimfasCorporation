package hu.nimfasco.casino.game.yahtzee;

import hu.nimfasco.casino.util.RandomGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dice {

    private Integer numOfSides;
    private Integer value;

    public Integer roll() {
        return RandomGenerator.randomInt(numOfSides);
    }
}
