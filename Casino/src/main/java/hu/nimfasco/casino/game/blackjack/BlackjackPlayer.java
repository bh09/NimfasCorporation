/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.util.List;

/**
 *
 * @author Zsófi
 */
public class BlackjackPlayer {
    private Hand hand = new Hand ();
    private double cash;
    
    BlackjackPlayer (double cash) {
        this.cash = cash;
    }
    BlackjackPlayer () {
        
    }

    @Override
    public String toString() {
        return "BlackjackPlayer{" + "hand=" + hand + ", cash=" + cash + '}';
    }

    public int getPoints() {
        return hand.CalculatePoints ();
    }

    public void addCard(Card card) {
        hand.AddCard(card);
    }
    
    public List<Card> getCards () {
        return hand.getCards ();
    }
    
    public Card getCard (int i) {
        return hand.getCards ().get (i);
    }

    public double getCash() {
        return cash;
    }

    public void increaseCash(double cash) {
        this.cash += cash;
    }
    
    public void decreaseCash(double cash) {
        this.cash -= cash;
    }
    
}
