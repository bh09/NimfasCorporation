package hu.nimfasco.casino.game.yahtzee;

import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.game.yahtzee.type.YahtzeeBoxValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class YahtzeeRound {

    private User user;
    private YahtzeeBoxValue boxValue;
    private Integer boxScore;

}
