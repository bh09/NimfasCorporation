/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Zsófi
 */
   

public class Hand {
    
    static Map<CardEnum,Integer> points ;
    static {
        points = new HashMap<>();
        points.put(CardEnum.KING, 10);
        points.put(CardEnum.ACE, 1);
        points.put(CardEnum.QUEEN, 10);
        points.put(CardEnum.JACK, 10);
        points.put(CardEnum.TEN, 10);
        points.put(CardEnum.NINE, 9);
        points.put(CardEnum.EIGHT, 8);
        points.put(CardEnum.SEVEN, 7);
        points.put(CardEnum.SIX, 6);
        points.put(CardEnum.FIVE, 5);
        points.put(CardEnum.FOUR, 4);
        points.put(CardEnum.THREE, 3);
        points.put(CardEnum.TWO, 2);
       
    }
    private List<Card> cards = new ArrayList<>();
        
    void AddCard (Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return "Hand{" + "cards=" + cards + '}';
    }
    
    int CalculatePoints () {
        int aces = 0;
        int result = 0;
        for (Card c : cards) {
            if (c.getValue() != CardEnum.ACE) {
                result += points.get(c.getValue());
            } else {
                ++aces;
            }       
        }
        if (aces == 2) {
            result += 12;
        } else if (aces == 1)  {
            if (result + 11 <= 21)
                result += 11;
            else
                result += 1;

        }
        return result;        
    }

    List<Card> getCards() {
        return cards;
    }
}
