/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.ejb.Singleton;

/**
 *
 * @author Zsófi
 */
@Singleton
public class SessionData {

    @Override
    public String toString() {
        return "SessionData{" + "userDetail_id=" + userDetail_id + ", session_id=" + session_id + ", sessionStart=" + sessionStart + ", session_achievement=" + session_achievement + '}';
    }
    
    private int userDetail_id;
    private int session_id;
    private LocalDate sessionStart;
    private int session_achievement;

    
    public int getUserDetail_id() {
        return userDetail_id;
    }

    public void setUserDetail_id(int userDetail_id) {
        this.userDetail_id = userDetail_id;
    }

    public int getSession_id() {
        return session_id;
    }

    public void setSession_id(int session_id) {
        this.session_id = session_id;
    }

    public LocalDate getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(LocalDate sessionStart) {
        this.sessionStart = sessionStart;
    }

    public int getSession_achievement() {
        return session_achievement;
    }

    public void setSession_achievement(int session_achievement) {
        this.session_achievement = session_achievement;
    }
    
}
