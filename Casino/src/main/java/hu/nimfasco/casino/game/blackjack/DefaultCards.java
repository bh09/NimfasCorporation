/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.util.ArrayList;
import java.util.List;

public class DefaultCards {

    public static List<Card> getDefaultCards() {
        List<Card> result = new ArrayList<>();
        result.add(new Card(CardEnum.ACE, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.KING, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.QUEEN, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.JACK, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.TWO, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.THREE, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.FOUR, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.FIVE, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.SIX, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.SEVEN, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.EIGHT, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.NINE, ColorEnum.CLUBS));
        result.add(new Card(CardEnum.TEN, ColorEnum.CLUBS));

        result.add(new Card(CardEnum.ACE, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.KING, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.QUEEN, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.JACK, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.TWO, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.THREE, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.FOUR, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.FIVE, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.SIX, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.SEVEN, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.EIGHT, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.NINE, ColorEnum.DIAMONDS));
        result.add(new Card(CardEnum.TEN, ColorEnum.DIAMONDS));

        result.add(new Card(CardEnum.ACE, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.KING, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.QUEEN, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.JACK, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.TWO, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.THREE, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.FOUR, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.FIVE, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.SIX, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.SEVEN, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.EIGHT, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.NINE, ColorEnum.HEARTS));
        result.add(new Card(CardEnum.TEN, ColorEnum.HEARTS));

        result.add(new Card(CardEnum.ACE, ColorEnum.SPADES));
        result.add(new Card(CardEnum.KING, ColorEnum.SPADES));
        result.add(new Card(CardEnum.QUEEN, ColorEnum.SPADES));
        result.add(new Card(CardEnum.JACK, ColorEnum.SPADES));
        result.add(new Card(CardEnum.TWO, ColorEnum.SPADES));
        result.add(new Card(CardEnum.THREE, ColorEnum.SPADES));
        result.add(new Card(CardEnum.FOUR, ColorEnum.SPADES));
        result.add(new Card(CardEnum.FIVE, ColorEnum.SPADES));
        result.add(new Card(CardEnum.SIX, ColorEnum.SPADES));
        result.add(new Card(CardEnum.SEVEN, ColorEnum.SPADES));
        result.add(new Card(CardEnum.EIGHT, ColorEnum.SPADES));
        result.add(new Card(CardEnum.NINE, ColorEnum.SPADES));
        result.add(new Card(CardEnum.TEN, ColorEnum.SPADES));
        return result;
    }
}
