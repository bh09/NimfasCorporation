/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.util.List;

/**
 *
 * @author Zsófi
 */
public class Deck {
    List<Card> cards;

    @Override
    public String toString() {
        return "Deck{" + "cards=" + cards + '}';
    }

    public Deck(List<Card> cards) {
        this.cards = cards;
    }
    
    public Card pop() {
       Card result = cards.get(0);
       cards.remove (0);
       return result;
    }
}
