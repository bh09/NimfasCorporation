package hu.nimfasco.casino.game.yahtzee.type;

/**
 *
 * @author horvathbzs
 */
public enum YahtzeeBoxValue {
    ACES(1), TWOS(2), THREES(3), FOURS(4), FIVES(5), SIXES(6),
    THREE_OF_A_KIND(3), FOUR_OF_A_KIND(4), FULL_HOUSE(5),
    SMALL_STRAIGHT(15), LARGE_STRAIGTH(20), YATHZEE(25), CHANCE(1);

    private final int valueMultiplier;
    
    private YahtzeeBoxValue(int boxValue) {
        this.valueMultiplier = boxValue;
    }

    public int getValueMultiplier() {
        return valueMultiplier;
    }
}
