package hu.nimfasco.casino.game.yahtzee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DiceHand {
    
    private Integer numOfDices;
    private List<Dice> dicesInHand = new ArrayList<>();
    private Map<Dice, Integer> countDistinctDices = new HashMap<>();
    
    public void add(Dice d) {
        dicesInHand.add(d);
    }
    
    public void changeOneDiceInHand(int index) {
         dicesInHand.get(index).setValue(dicesInHand.get(index).roll());
    }
}
