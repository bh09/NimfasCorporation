/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import hu.nimfasco.casino.dao.impl.SessionDao;
import hu.nimfasco.casino.dao.impl.UserDao;
import hu.nimfasco.casino.dao.impl.UserDetailDao;
import hu.nimfasco.casino.dto.SessionDto;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.entity.Session;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import hu.nimfasco.casino.gameEnum.GameEnum;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.inject.Inject;


@Stateful
public class BlackJackGame {
    private static int gameId = 0;
    private final static int DefaultCash = 2000;
    private BlackjackPlayer player;
    private BlackjackPlayer dealer;
    private Deck deck;
    private int bet;
    private boolean stayed;
    
    private Session sessionData = new Session (); 


    @Inject
    private UserDao userDao;
    
    @Inject
    private SessionDao sessionDao;
    //megkeverjük a kértyákat, minden egyes randomizálással 
    //egyre rövidebb lesz a lista, és mindig a lista méretéhez kötjük, milyen számot randomolunk

    public static int getGameId() {
        return gameId;
    }

    public static void setGameId(int gameId) {
        BlackJackGame.gameId = gameId;
    }

    public BlackjackPlayer getPlayer() {
        return player;
    }

    public void setPlayer(BlackjackPlayer player) {
        this.player = player;
    }

    public BlackjackPlayer getDealer() {
        return dealer;
    }

    public void setDealer(BlackjackPlayer dealer) {
        this.dealer = dealer;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public boolean isStayed() {
        return stayed;
    }

    public void setStayed(boolean stayed) {
        this.stayed = stayed;
    }

    public Session getSessionData() {
        return sessionData;
    }

    public void setSessionData(Session sessionData) {
        this.sessionData = sessionData;
    }
    
    
    public static List<Card>getRandomCard() {
        
        List<Card> list = DefaultCards.getDefaultCards();
        
        List<Card> result = new ArrayList<>();
        
        while (list.size() > 0) {
            int rand = (int)(Math.random() * list.size()-1);
            result.add(list.get(rand));
            list.remove(rand);
        }
        return result;
    } 

    @Override
    public String toString() {
        return "BlackJackGame{" + "player=" + player + ", dealer=" + dealer + ", deck=" + deck + ", bet=" + bet + ", stayed=" + stayed + '}';
    }
        
              
        public void initializeGame() {
            player = new BlackjackPlayer(sessionData.getSessionAchivement());
            bet = 0;
            dealer = new BlackjackPlayer();
            deck = new Deck (getRandomCard());
            
            player.addCard(deck.pop ());
            player.addCard(deck.pop ());
            
            dealer.addCard(deck.pop ());
            dealer.addCard(deck.pop ());
            
            stayed = false;
        }
        
        public boolean increaseBet(int number) {
            if (number < player.getCash()) {
                bet += number;
                player.decreaseCash(number);
                return true;
            }
            return false;
        }
        
        public void decreaseBet(int number) {
            if (bet >= number) {
                bet -= number;
                player.increaseCash(number);
            }
        }
        
        public void doHit() {
            if (evaluateGameState() != WonEnum.GAMEISINPROGRESS)
                return;
            player.addCard(deck.pop());
        }
        
        public void doStay() {
            while (dealer.getPoints () <= 16) {
                dealer.addCard(deck.pop ());
            }
            stayed = true;
        }
        
        
        //kiértékelés
        public WonEnum evaluateGameState () {
            if (player.getPoints() > 21) {
                return WonEnum.LOOSE;
            }
            if (player.getPoints () == 21) {
                player.increaseCash(bet*2);
                return WonEnum.WON;
            }
            if (stayed) {
                if (dealer.getPoints() > 21) {
                    player.increaseCash(bet*2);
                    return WonEnum.WON;
                } else {
                    if (player.getPoints() > dealer.getPoints()) {
                        player.increaseCash(bet*2);
                    
                        return WonEnum.WON;
                    }
                    else if (player.getPoints() < dealer.getPoints()) {
                        return WonEnum.LOOSE;
                    }
                    else {
                        player.increaseCash(bet);
                        return WonEnum.DRAW;
                    }
                }
            }
            return WonEnum.GAMEISINPROGRESS;
        }
        public List<Card> getPlayerCards () {
            return player.getCards();
        }
        
        public List<Card> getDealerCards () {
            return dealer.getCards();
        }
        public List<Card> getDealerCard () {
            List<Card> result = new ArrayList<>();
            result.add(dealer.getCard(0));
            return result;
        }
        public int getBet () {
            return bet;
        }
        public double getPlayerCash () {
            return player.getCash();
        }
        
        public void initializeGameData (LocalDate actualDate, String userEmail) {
            
           
             User user = userDao.get(userEmail).get();
            
            
          
            sessionData.setUserDetail(user.getDetails());
            sessionData.setGame(GameEnum.BLACKJACK);
            sessionData.setSessionStart(actualDate);
        }
        
        public void saveData () {
            
            sessionData.setSessionEnd(LocalDate.now());
            sessionData.setSessionAchivement(player.getCash());         
        }
        
        public void writeAllToDatabase () {
           
           sessionDao.save(sessionData);
        }
}
