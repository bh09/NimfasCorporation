/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.web.registration;

import hu.nimfasco.casino.constant.WebPagesConstantsIndex;
import hu.nimfasco.casino.constant.WebPagesConstantsLogin;
import hu.nimfasco.casino.constant.WebPagesConstantsRegistration;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.service.registration.RegistrationService;
import hu.nimfasco.casino.util.VerifyRecaptcha;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import static jdk.nashorn.internal.objects.NativeError.getFileName;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zsófi
 */
@MultipartConfig(location = "/img", fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet(name = WebPagesConstantsRegistration.SERVLET_REGISTRATION,
        urlPatterns = {WebPagesConstantsRegistration.URL_PATTERN_REGISTRATION})
public class RegistrationServlet extends HttpServlet {

    private final static Logger LOGGER = LoggerFactory.getLogger(RegistrationServlet.class);

    @Inject
    RegistrationService registrationService;

    private static final long serialVersionUID = -6506682026701304964L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("captchaerror", "");

        request.getRequestDispatcher(WebPagesConstantsRegistration.PATH_REGISTRATION).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (!request.getParameter("password").equals(request.getParameter("password2"))) {
            request.setAttribute("passwordcompareerror", "Nem egyezik a két érték!");
            request.getRequestDispatcher(WebPagesConstantsRegistration.PATH_REGISTRATION).forward(request, response);
        } else {
            request.setAttribute("passwordcompareerror", "");
            final String fileName = request.getParameter("username") + "_pict.jpg";
            UserDetailDto userDetailDto = registrationService.setUserDetailDto(request.getParameter("username"), fileName);
            UserDto userDto = registrationService.setUserDto(request.getParameter("email"), request.getParameter("password"), userDetailDto);
            userDetailDto.setUserDto(userDto);

            final String path = "C:\\Casino\\img";
            final Part filePart = request.getPart("file");
            
            OutputStream out = null;
            InputStream filecontent = null;
            try {
                out = new FileOutputStream(new File(path + File.separator
                        + fileName));
                filecontent = filePart.getInputStream();

                int read = 0;
                final byte[] bytes = new byte[1024];

                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }

            } catch (FileNotFoundException fne) {

            } finally {
                if (out != null) {
                    out.close();
                }
                if (filecontent != null) {
                    filecontent.close();
                }
            }

            try {
                String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
                System.out.println(gRecaptchaResponse);
                boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
                if (verify) {
                    request.setAttribute("captchaerror", "");
                    registrationService.savePlayer(userDto);
                    LOGGER.info(userDto.getEmail() + " user registered");

                    response.sendRedirect("index.jsp");

                } else {
                   request.setAttribute("captchaerror", "You missed the Captcha.");
                     request.getRequestDispatcher(WebPagesConstantsRegistration.PATH_REGISTRATION).forward(request, response);
		}
                
		
            
        } catch (IllegalAccessException ex) {
            LOGGER.debug(ex.toString());
        } catch (InvocationTargetException ex) {
            LOGGER.debug(ex.toString());
        }    
                  
        }

    }
}
