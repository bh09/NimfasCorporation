package hu.nimfasco.casino.web.admin;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsAdmin;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author horvathbzs
 */
@WebServlet(name = WebPagesConstantsAdmin.SERVLET_ADMIN_SEND_EMAIL,
        urlPatterns = {WebPagesConstantsAdmin.URL_PATTERN_ADMIN_SEND_EMAIL})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE}))
public class AdminSendEmailServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.getRequestDispatcher(WebPagesConstantsAdmin.PATH_ADMIN_SEND_EMAIL).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
