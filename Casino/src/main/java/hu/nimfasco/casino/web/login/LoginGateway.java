/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.web.login;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsAdmin;
import hu.nimfasco.casino.constant.WebPagesConstantsGame;
import hu.nimfasco.casino.constant.WebPagesConstantsLogin;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author vbali
 */
@WebServlet(name = WebPagesConstantsLogin.SERVLET_GATEWAY,
        urlPatterns = {WebPagesConstantsLogin.URL_PATTERN_GATEWAY})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE, RoleConstants.PLAYER_ROLE}))
public class LoginGateway extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.isUserInRole("admin")) {
            request.getRequestDispatcher(WebPagesConstantsAdmin.URL_PATTERN_ADMIN).forward(request, response);
        } else if (request.isUserInRole("player")) {
            //request.setAttribute("userEmail", null /*userből le kell kérni*/);
            request.getRequestDispatcher(WebPagesConstantsGame.PATH_NEW_GAME).forward(request, response);
        } else {
            request.getRequestDispatcher(WebPagesConstantsLogin.URL_PATTERN_GATEWAY).forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
