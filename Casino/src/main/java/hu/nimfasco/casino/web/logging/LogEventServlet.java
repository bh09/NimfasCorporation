package hu.nimfasco.casino.web.logging;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.LoggingConstants;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import hu.nimfasco.casino.constant.WebPagesConstantsLogging;
import hu.nimfasco.casino.dto.LogEventDto;
import hu.nimfasco.casino.service.logging.LogEventService;
import hu.nimfasco.casino.util.LogReader;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;

/**
 *
 * @author horvathbzs
 */
@WebServlet(name = WebPagesConstantsLogging.SERVLET_LOG_EVENT ,
        urlPatterns = {WebPagesConstantsLogging.URL_PATTERN_LOG_EVENT})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE}))
public class LogEventServlet extends HttpServlet {
    
    @Inject
    private LogEventService logEventService;
    
    //private List<LogEventDto> logEventList = logEventService.readLogFile(LoggingConstants.PATH_LOGFILE);
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //List<String> logEventList = LogReader.readLogFile(LoggingConstants.PATH_LOGFILE);
        
        List<LogEventDto> logEventList = logEventService.readLogFile(LoggingConstants.PATH_LOGFILE);
        
        request.setAttribute("logEventList", logEventList);
        
        request.getRequestDispatcher(WebPagesConstantsLogging.PATH_LOG_EVENT).forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
//        String level = request.getParameter("select_level");
//        
//        List<LogEventDto> filteredLogEventList = logEventService.getLogEventsByLevel(logEventList, level);
//        
//        
//        request.setAttribute("logEventList", filteredLogEventList);
//        
//        request.getRequestDispatcher(WebPagesConstantsLogging.PATH_LOG_EVENT).forward(request, response);
    }
}
