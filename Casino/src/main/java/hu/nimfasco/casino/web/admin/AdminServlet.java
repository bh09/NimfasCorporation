package hu.nimfasco.casino.web.admin;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsAdmin;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.service.admin.AdminService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author horvathbzs
 */
@WebServlet(name = WebPagesConstantsAdmin.SERVLET_ADMIN,
        urlPatterns = {WebPagesConstantsAdmin.URL_PATTERN_ADMIN})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE}))
public class AdminServlet extends HttpServlet {
    
    @Inject
    AdminService adminService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<UserDetailDto> userDetailDtos = adminService.getPlayers();
        request.setAttribute("userDetail", adminService.getPlayers());

        request.getRequestDispatcher(WebPagesConstantsAdmin.PATH_ADMIN).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
