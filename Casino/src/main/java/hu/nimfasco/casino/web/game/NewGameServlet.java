package hu.nimfasco.casino.web.game;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsGame;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author horvathbzs
 */
@WebServlet(name = WebPagesConstantsGame.SERVLET_NEW_GAME,
        urlPatterns = {WebPagesConstantsGame.URL_PATTERN_NEW_GAME})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE, RoleConstants.PLAYER_ROLE}))
public class NewGameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(WebPagesConstantsGame.PATH_NEW_GAME).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
