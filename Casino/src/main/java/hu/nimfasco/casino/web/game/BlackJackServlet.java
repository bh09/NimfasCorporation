/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.web.game;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsGame;
import hu.nimfasco.casino.dao.impl.SessionDao;
import hu.nimfasco.casino.dao.impl.UserDetailDao;
import hu.nimfasco.casino.dto.SessionDto;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.entity.Session;
import hu.nimfasco.casino.entity.UserDetail;
import hu.nimfasco.casino.game.blackjack.BlackJackGame;
import hu.nimfasco.casino.game.blackjack.SessionData;
import hu.nimfasco.casino.game.blackjack.WonEnum;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author vbali
 */
@WebServlet(name = WebPagesConstantsGame.SERVLET_BLACKJACK, urlPatterns = {WebPagesConstantsGame.URL_PATTERN_BLACKJACK})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE, RoleConstants.PLAYER_ROLE}))
public class BlackJackServlet extends HttpServlet {

    @Inject
    private BlackJackGame game;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       
        game.initializeGameData(LocalDate.now(),request.getRemoteUser()); 
        game.initializeGame();
        
        request.getRequestDispatcher(WebPagesConstantsGame.PATH_BLACKJACK).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        if (request.getParameter("increaseBet") != null) {
            game.increaseBet(50);
        } else if (request.getParameter("decreaseBet") != null) {
            game.decreaseBet(50);
        } else if (request.getParameter("hit") != null) {
            game.doHit();
        } else if (request.getParameter("stand") != null) {
            game.doStay();
        } else if (request.getParameter("deal") != null) {    
            game.initializeGame();
        } else {
            
          game.writeAllToDatabase();
          request.getRequestDispatcher(WebPagesConstantsGame.PATH_NEW_GAME).forward(request, response);
        }
        WonEnum state = game.evaluateGameState();
        if (state != WonEnum.GAMEISINPROGRESS) {
            game.saveData ();
        }
        System.out.println (state);
        System.out.println(game);
       /* Userdetaildto dto = game.getszükségesuserdetaildto ();
         request.setAttribute("email", dto.getEmail ());
        request.setAttribute("plapictureyerCash", dto.getKepValami());
        
        */
        request.setAttribute("bet", game.getBet());
        request.setAttribute("playerCash", game.getPlayerCash());
        request.setAttribute("playerCards", game.getPlayerCards());
           
        request.setAttribute("isGameInProgress", WonEnum.GAMEISINPROGRESS.equals(state));
        
        if(!WonEnum.GAMEISINPROGRESS.equals(state)) {
            request.setAttribute("state", "YOU " + state + "!");
            request.setAttribute("dealerCards", game.getDealerCards());       
        } else {
            request.setAttribute("dealerCards", game.getDealerCard());      
        }

        request.getRequestDispatcher(WebPagesConstantsGame.PATH_BLACKJACK).forward(request, response);
    }

}
