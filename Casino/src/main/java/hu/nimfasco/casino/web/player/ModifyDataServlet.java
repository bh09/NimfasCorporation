/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.web.player;

import hu.nimfasco.casino.constant.RoleConstants;
import hu.nimfasco.casino.constant.WebPagesConstantsIndex;
import hu.nimfasco.casino.constant.WebPagesConstantsPlayer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Zsófi
 */
@WebServlet(name = WebPagesConstantsPlayer.SERVLET_MODIFYDATA,
        urlPatterns = {WebPagesConstantsPlayer.URL_PATTERN_MODIFYDATA})
@ServletSecurity(@HttpConstraint(rolesAllowed = {RoleConstants.ADMIN_ROLE, RoleConstants.PLAYER_ROLE}))
public class ModifyDataServlet extends HttpServlet {

    
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(WebPagesConstantsPlayer.PATH_MODIFYDATA).forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(WebPagesConstantsPlayer.PATH_MODIFYDATA).forward(request, response);
    }

  

}
