package hu.nimfasco.casino.dao.impl;

import hu.nimfasco.casino.dao.Dao;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 */

@Stateless
@LocalBean
public class UserDao implements Dao<User> {

    @PersistenceContext(unitName = "CasinoPU")
    EntityManager em;
    
    @Override
    public List<User> getAll() {
        Query q = em.createQuery("Select u from User u", User.class);
        return q.getResultList();
    }

    @Override
    public void save(User u) {
        em.persist(u);
    }

    @Override
    public void delete(User u) {
        em.remove(u);
    }

    @Override
    public Optional<User> get(Long id) {
        return Optional.ofNullable(em.find(User.class,id));
    }
    
    public Optional<User> get(String id) {
        return Optional.ofNullable(em.find(User.class,id));
    }

    @Override
    public void update(User u, String[] params) {
//        User userToUpdate = em.find(User.class,u.getUserId());
//        userToUpdate.setEmail(params[0]);
//        userToUpdate.setPassword(params[1]);
//        em.merge(userToUpdate);
    }

    @Override
    public List<User> getEntityByColumn(Class<User> entityClass, String colName, String value) {
       Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value ));
       return q.setParameter(PARAM_VALUE, value).getResultList();
    }
}
