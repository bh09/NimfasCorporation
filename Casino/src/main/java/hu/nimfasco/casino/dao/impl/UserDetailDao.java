/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dao.impl;

import hu.nimfasco.casino.constant.QueryConstantsForAdmin;
import hu.nimfasco.casino.dao.Dao;
import hu.nimfasco.casino.entity.UserDetail;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vbali
 */
@Stateless
@LocalBean
public class UserDetailDao implements Dao<UserDetail> {
    
    
    @PersistenceContext(unitName = "CasinoPU")
    EntityManager em;

    @Override
    public List<UserDetail> getAll() {
        Query q = em.createQuery("Select u from UserDetail u", UserDetail.class);
        return q.getResultList();
    }

    @Override
    public void save(UserDetail u) {
        em.persist(u);
    }

    @Override
    public void delete(UserDetail u) {
        em.remove(u);
    }

    @Override
    public Optional<UserDetail> get(Long id) {
        return Optional.ofNullable(em.find(UserDetail.class, id));
    }

    @Override
    public void update(UserDetail u, String[] params) {
        UserDetail userDetailToUpdate = em.find(UserDetail.class, u.getDetailId());
        userDetailToUpdate.setPlayerName(params[0]);
        userDetailToUpdate.setBalance(Double.parseDouble(params[1]));
        userDetailToUpdate.setPhotopath(params[0]);
        em.merge(userDetailToUpdate);
    }

    public void banUpdate(UserDetail userDetail) {
        UserDetail userDetailToUpdate = em.find(UserDetail.class, userDetail.getDetailId());
        if (userDetail.isBanned() == false) {
            userDetail.setBanned(true);
        } else {
            userDetail.setBanned(false);
        }
    }

    @Override
    public List getEntityByColumn(Class<UserDetail> entityClass, String colName, String value) {
        Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value));
        return q.setParameter(PARAM_VALUE, value).getResultList();
    }
    
    public List<UserDetail> getPlayers(){
        return em.createQuery(QueryConstantsForAdmin.JPQL_FILTER_PLAYERS_QUERY + QueryConstantsForAdmin.PARAM_NAME, UserDetail.class)
            .setParameter(QueryConstantsForAdmin.PARAM_NAME, QueryConstantsForAdmin.PLAYER)
            .getResultList();
    
       
    }

}
