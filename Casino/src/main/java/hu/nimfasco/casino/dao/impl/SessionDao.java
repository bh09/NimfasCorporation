/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dao.impl;

import hu.nimfasco.casino.dao.Dao;
import hu.nimfasco.casino.dto.SessionDto;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.entity.Session;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import hu.nimfasco.casino.gameEnum.GameEnum;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vbali
 */
@Stateless
@LocalBean
public class SessionDao implements Dao<Session> {

    @PersistenceContext(unitName = "CasinoPU")
    EntityManager em;
    
    @Override
    public List<Session> getAll() {
        Query q = em.createQuery("Select s from Session s", Session.class);
        return q.getResultList();
    }

    @Override
    public void save(Session s) {
        em.persist(s);
    }

    @Override
    public void delete(Session s) {
        em.remove(s);
    }

    @Override
    public Optional<Session> get(Long id) {
        return Optional.ofNullable(em.find(Session.class,id));
    }

    @Override
    public void update(Session s, String[] params) {
        Session sessionToUpdate = em.find(Session.class,s.getSessionId());
        sessionToUpdate.setGame(GameEnum.BLACKJACK);
        sessionToUpdate.setSessionAchivement(Double.parseDouble(params[1]));
        em.merge(sessionToUpdate);
    }

    @Override
    public List<Session> getEntityByColumn(Class<Session> entityClass, String colName, String value) {
        Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value ));
       return q.setParameter(PARAM_VALUE, value).getResultList();
    }
    
    static long i = 0;
    
    public void save(SessionDto sessionDto) {
        UserDetailDto userDto = sessionDto.getUser();
        UserDetail user = new UserDetail (userDto);
        
        Session session = new Session (i++, user, GameEnum.BLACKJACK, sessionDto.getSessionStart(), sessionDto.getSessionEnd(), sessionDto.getSessionAchivement());
        save(session);
    }
    
}
