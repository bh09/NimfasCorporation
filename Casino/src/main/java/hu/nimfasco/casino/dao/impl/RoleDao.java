/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.dao.impl;

import hu.nimfasco.casino.dao.Dao;
import static hu.nimfasco.casino.dao.Dao.JPQL_GET_ENTITY_BY_COLNAME;
import static hu.nimfasco.casino.dao.Dao.PARAM_VALUE;
import hu.nimfasco.casino.entity.Role;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vbali
 */
@Stateless
@LocalBean
public class RoleDao implements Dao<Role>{

    @PersistenceContext(unitName = "CasinoPU")
    EntityManager em;
    
    @Override
    public List<Role> getAll() {
        Query q = em.createQuery("Select r from Role r", Role.class);
        return q.getResultList();
    }

    @Override
    public void save(Role r) {
        em.persist(r);
    }

    @Override
    public void delete(Role r) {
        em.remove(r);
    }

    @Override
    public Optional<Role> get(Long id) {
        return Optional.ofNullable(em.find(Role.class,id));
    }

    @Override
    public void update(Role r, String[] params) {
//        Role roleToUpdate = em.find(Role.class,r.getRoleId());
//        roleToUpdate.setRoleId(Long.parseLong(params[0]));
//        roleToUpdate.setRoleName(params[1]);
//        em.merge(roleToUpdate);
    }

    @Override
    public List<Role> getEntityByColumn(Class<Role> entityClass, String colName, String value) {
        Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value ));
       return q.setParameter(PARAM_VALUE, value).getResultList();
    }
    
}
