package hu.nimfasco.casino.dao.impl;

import hu.nimfasco.casino.dao.Dao;
import static hu.nimfasco.casino.dao.Dao.JPQL_GET_ENTITY_BY_COLNAME;
import static hu.nimfasco.casino.dao.Dao.PARAM_VALUE;
import hu.nimfasco.casino.entity.LogEvent;
import hu.nimfasco.casino.entity.User;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 */

@Stateless
@LocalBean
public class LogEventDao implements Dao<LogEvent> {
    
    @PersistenceContext(unitName = "CasinoPU")
    EntityManager em;
    
    @Override
    public List<LogEvent> getAll() {
        Query q = em.createQuery("Select le from Logevent le", LogEvent.class);
        return q.getResultList();
    }

    @Override
    public void save(LogEvent logEvent) {
        em.merge(logEvent);
    }

    @Override
    public void delete(LogEvent logEvent) {
        em.remove(logEvent);
    }

    @Override
    public Optional<LogEvent> get(Long id) {
        return Optional.ofNullable(em.find(LogEvent.class,id));
    }

    @Override
    public void update(LogEvent logEvent, String[] params) {
        LogEvent logEventToUpdate = em.find(LogEvent.class,logEvent.getId());
        logEventToUpdate.setLogDateTime(LocalDate.parse(params[0]));
        logEventToUpdate.setLogLevel(params[1]);
        logEventToUpdate.setLogMessage(params[3]);
        em.merge(logEventToUpdate);
    }

    @Override
    public List<LogEvent> getEntityByColumn(Class<LogEvent> entityClass, String colName, String value) {
       Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value ));
       return q.setParameter(PARAM_VALUE, value).getResultList();
    }
}
