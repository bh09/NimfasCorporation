package hu.nimfasco.casino.game.yahtzee;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Answers;
import org.mockito.Mock;

/**
 *
 * @author horvathbzs
 */
public class YahzteeScoreCardTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private YahtzeeRound yahtzeeRound;

    private YahtzeeScoreCard underTest;

    @BeforeEach
    public void setupYahtzeeScoreCard() {
        underTest = new YahtzeeScoreCard();
    }

    @Test
    public void testAdd() {
        assertAll("YahtzeeRoundsInYahtzeeScoreCardListSize",
                () -> {
                    //When
                    underTest.add(yahtzeeRound);

                    //Then
                    assertEquals(1, underTest.getYathzeeRounds().size());
                },
                () -> {
                    //When
                    underTest.add(yahtzeeRound);

                    //Then
                    assertEquals(2, underTest.getYathzeeRounds().size());
                }
        );
    }
}
