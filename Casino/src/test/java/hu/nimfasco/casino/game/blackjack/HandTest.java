/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;
/**
 *
 * @author Zsófi
 */
public class HandTest {
    
    public HandTest() {
    }

    @Test
    public void testCalculate1() {
        Hand hand = new Hand ();
        Card card = new Card (CardEnum.TWO, ColorEnum.CLUBS);
        
        hand.AddCard(card);
        hand.AddCard(card);
        
        assertEquals(hand.CalculatePoints(), 4);
    }
    
    @Test
    public void testCalculateAce1() {
        Hand hand = new Hand ();
        Card card = new Card (CardEnum.ACE, ColorEnum.CLUBS);
        
        hand.AddCard(card);
        hand.AddCard(card);
        
        assertEquals(hand.CalculatePoints(), 12);
    }
    
    @Test
    public void testCalculateAce2() {
        Hand hand = new Hand ();
       
        hand.AddCard(new Card (CardEnum.ACE, ColorEnum.CLUBS));
        hand.AddCard(new Card (CardEnum.TWO, ColorEnum.CLUBS));
        
        assertEquals(hand.CalculatePoints(), 13);
    }
    
    @Test
    public void testCalculateAce3() {
        Hand hand = new Hand ();
       
        hand.AddCard(new Card (CardEnum.ACE, ColorEnum.CLUBS));
        hand.AddCard(new Card (CardEnum.KING, ColorEnum.CLUBS));
        hand.AddCard(new Card (CardEnum.TWO, ColorEnum.CLUBS));
        
        assertEquals(hand.CalculatePoints(), 13);
    }
    
}
