package hu.nimfasco.casino.game.yahtzee;

/**
 *
 * @author horvathbzs
 */
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Answers;
import org.mockito.Mock;

public class DiceHandTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Dice dice;

    private DiceHand underTest;

    @BeforeEach
    public void setupDiceHand() {
        underTest = new DiceHand();
    }

    @Test
    public void testAdd() {
        assertAll("DiceInHandListSize",
                () -> {
                    //When
                    underTest.add(dice);

                    //Then
                    assertEquals(1, underTest.getDicesInHand().size());
                },
                () -> {
                    //When
                    underTest.add(dice);

                    //Then
                    assertEquals(2, underTest.getDicesInHand().size());
                }
        );
    }
}
