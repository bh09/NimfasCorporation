/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.game.blackjack;

import java.time.LocalDate;
import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/**
 *
 * @author Zsófi
 */
public class BlackJackGameTest {
    BlackJackGame game;
    public BlackJackGameTest() {
    }
    @BeforeEach
    public void beforeEachTestMethod() {
       game = new BlackJackGame();
        game.setPlayer(new BlackjackPlayer(2000));
        game.setDealer(new BlackjackPlayer());
        game.setDeck(new Deck (game.getRandomCard()));
        
            
            game.getPlayer().addCard(game.getDeck().pop ());
            game.getPlayer().addCard(game.getDeck().pop ());
            
            game.getDealer().addCard(game.getDeck().pop ());
            game.getDealer().addCard(game.getDeck().pop ());
            
            game.setStayed(false);
       
    }
    @Test
    public void testIncreaseBet() {
        game.increaseBet(50);
        
        assertEquals(game.getBet(), 50);
        assertTrue(game.getPlayerCash() == 1950.0);
    }
    
     @Test
    public void testDecreaseBet() {
       game.decreaseBet(50);
        
        assertEquals(game.getBet(), 0);
        assertTrue(game.getPlayerCash() == 2000.0);
        
        game.increaseBet(100);
        game.decreaseBet(50);
        
        assertEquals(game.getBet(), 50);
        assertTrue(game.getPlayerCash() == 1950.0);
        
        
        
    }
    
     @Test
    public void testEvaluateGame() {
        game.getPlayerCards().clear();
        
        game.getPlayerCards().add (new Card (CardEnum.TWO, ColorEnum.CLUBS));
        assertEquals(game.evaluateGameState(), WonEnum.GAMEISINPROGRESS);
        
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
               
        assertEquals(game.evaluateGameState(), WonEnum.LOOSE);
          
    }
    
    @Test
    public void testEvaluateGame2() {
        game.getPlayerCards().clear();
         game.getDealerCards().clear();
        
        game.getDealerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.getDealerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        assertEquals(game.evaluateGameState(), WonEnum.GAMEISINPROGRESS);
        
        game.getPlayerCards().add (new Card (CardEnum.NINE, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.NINE, ColorEnum.CLUBS));
        game.doStay();
               
        assertEquals(game.evaluateGameState(), WonEnum.LOOSE);
          
    }
    
     @Test
    public void testEvaluateGame3() {
        game.getPlayerCards().clear();
        game.getDealerCards().clear();
        
        game.getDealerCards().add (new Card (CardEnum.NINE, ColorEnum.CLUBS));
        game.getDealerCards().add (new Card (CardEnum.NINE, ColorEnum.CLUBS));
        assertEquals(game.evaluateGameState(), WonEnum.GAMEISINPROGRESS);
        
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.doStay();
               
        assertEquals(game.evaluateGameState(), WonEnum.WON);
          
    }
    
    @Test
    public void testEvaluateGame4() {
        game.getPlayerCards().clear();
        
        game.getPlayerCards().add (new Card (CardEnum.TEN, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.NINE, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.TWO, ColorEnum.CLUBS));
               
        assertEquals(WonEnum.WON, game.evaluateGameState());
          
    }
    
    @Test
    public void testEvaluateGame41() {
        game.getPlayerCards().clear();
        
        game.getPlayerCards().add (new Card (CardEnum.TEN, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.TEN, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.ACE, ColorEnum.CLUBS));
               
        assertEquals(WonEnum.WON, game.evaluateGameState());
          
    }
    
     @Test
    public void testEvaluateGame5() {
        game.getPlayerCards().clear();
        game.getDealerCards().clear();
        
         game.getDealerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.getDealerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.getPlayerCards().add (new Card (CardEnum.KING, ColorEnum.CLUBS));
        game.doStay();
        assertEquals(game.evaluateGameState(), WonEnum.DRAW);
          
    }
    
}
