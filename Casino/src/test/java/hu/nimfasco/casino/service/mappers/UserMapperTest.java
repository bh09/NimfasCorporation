/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nimfasco.casino.service.mappers;

import hu.nimfasco.casino.dto.RoleDto;
import hu.nimfasco.casino.dto.UserDetailDto;
import hu.nimfasco.casino.dto.UserDto;
import hu.nimfasco.casino.entity.User;
import hu.nimfasco.casino.entity.UserDetail;
import static hu.nimfasco.casino.service.mappers.UserMapper.mapUserDetailFromUserDetailDto;
import static hu.nimfasco.casino.service.mappers.UserMapper.mapUserFromUserDto;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author vbali
 */
@ExtendWith(MockitoExtension.class)
public class UserMapperTest {
    
    @Test
    public void testMapUserDetailFromUserDetailDto() {
        //Given
        UserDetailDto userDetailDto = Mockito.mock(UserDetailDto.class);
        
        //When
        UserDetail userDetail = mapUserDetailFromUserDetailDto(userDetailDto);
        
        //Then
        assertNotNull(userDetail);
        
    }
    
    @Test
    public void testMapUserFromUserDto() {
        //Given
        UserDetailDto userDetailDto = new UserDetailDto();
        UserDto userDto = new UserDto();
        RoleDto roleDto = Mockito.mock(RoleDto.class);
        
        userDetailDto.setUserDto(userDto);
        userDto.setDetails(userDetailDto);
        userDto.setRole(roleDto);

        //When
        User user = mapUserFromUserDto(userDto);
        
        //Then
        assertNotNull(user);
    }
    
}
